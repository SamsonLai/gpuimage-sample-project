//
//  JGViewController.h
//  GPUImageProject
//
//  Created by Jacob Gundersen on 5/2/12.
//  Copyright (c) 2012 Interrobang Software LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JGViewController : UIViewController

@end
