//
//  JGViewController.m
//  GPUImageProject
//
//  Created by Jacob Gundersen on 5/2/12.
//  Copyright (c) 2012 Interrobang Software LLC. All rights reserved.
//

#import "JGViewController.h"
#import "GPUImage.h"
#import "GPUImagePolarPixellatePosterizeFilter.h"

@interface JGViewController () {
    GPUImageVideoCamera *vc;
    GPUImagePolarPixellatePosterizeFilter *ppf;
    GPUImageMovieWriter *movieWriter;
}

@end


@implementation JGViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    vc = [[GPUImageVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPreset640x480 cameraPosition:AVCaptureDevicePositionBack ];
    vc.outputImageOrientation = UIInterfaceOrientationPortrait;
    ppf = [[GPUImagePolarPixellatePosterizeFilter alloc] init];
    [vc addTarget:ppf];
    GPUImageView *v = [[GPUImageView alloc] init];
    [ppf addTarget:v];
    self.view = v;

    [vc startCameraCapture];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint location = [[touches anyObject] locationInView:self.view];
    CGSize pixelS = CGSizeMake(location.x / self.view.bounds.size.width * 0.1, location.y / self.view.bounds.size.height * 0.1) ;
    [ppf setPixelSize:pixelS];
}


-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint location = [[touches anyObject] locationInView:self.view];
    CGSize pixelS = CGSizeMake(location.x / self.view.bounds.size.width * 0.1, location.y / self.view.bounds.size.height * 0.1);
    [ppf setPixelSize:pixelS];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if ([self isIPhone]) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}


- (BOOL)shouldAutorotate {
    return YES;
}


- (NSUInteger)supportedInterfaceOrientations {
    if ([self isIPhone]) {
        return UIInterfaceOrientationMaskPortrait |
               UIInterfaceOrientationMaskPortraitUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}


- (BOOL)isIPhone {
   return [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone;
}


@end
